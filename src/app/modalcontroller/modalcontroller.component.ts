import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modalcontroller',
  templateUrl: './modalcontroller.component.html',
  styleUrls: ['./modalcontroller.component.css']
})
export class ModalcontrollerComponent implements OnInit {
  occupancyName: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,public dialogRef: MatDialogRef<ModalcontrollerComponent>,
  public route:Router,
  public cs: CommonService) { }

  showOccupancy:boolean=true;

  Data: any = [];
  search: any = '';
  ngOnInit() {
    this.Data = this.data.data
  }

  selectName(name: any) {
   this.cs.occupancyName = name;
   this.showOccupancy=false;
   this.dialogRef.close(name);
   this.route.navigateByUrl('/easy-protect')
  }

}
