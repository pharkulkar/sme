import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalcontrollerComponent } from './modalcontroller.component';

describe('ModalcontrollerComponent', () => {
  let component: ModalcontrollerComponent;
  let fixture: ComponentFixture<ModalcontrollerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalcontrollerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalcontrollerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
