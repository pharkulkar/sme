import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { ToastrService } from 'ngx-toastr';
import { promise } from 'protractor';
@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.component.html',
  styleUrls: ['./payment-confirmation.component.css']
})
export class PaymentConfirmationComponent implements OnInit {
  paymentConfRes: any; payment_confirmation: any; burglaryConfRes: any; showConfirmationDetails: boolean = false;
  imageArray = []; previewImageUrl: any;
  proposal_request_data: any;
  CpiNumber: any;
  CpiNumber_fire: any;
  CpiNumber_burglary: any;
  constructor(public cs: CommonService, private toastr: ToastrService) {
    this.showConfirmationDetails = false;
    this.cs.showLoader = true;
    this.proposal_request_data = JSON.parse(atob(localStorage.getItem('proposalRequest')));
  }

  ngOnInit() {
    let data = JSON.parse(atob(localStorage.getItem('razorPayData')));
    this.callPaymentConfirmation(data);
  }

  callPaymentConfirmation(data) {
    this.cs.getUserData('/api/Payment/SmeCommonPaymentConfirmationByPID?EPaymentID=' + data.epaymentId).then((res: any) => {
      // console.log(res);
      this.paymentConfRes = res.ConfirmPolicy[0];
      if (res.ConfirmPolicy.length > 1) {
        this.burglaryConfRes = res.ConfirmPolicy[1];
        if (this.burglaryConfRes.CpiNumber== null || this.burglaryConfRes.CpiNumber==undefined || this.burglaryConfRes.CpiNumber=='') {
          this.generateCPINumber(this.burglaryConfRes.PolicyID, 'burglary');
        }
        else {
          this.CpiNumber_burglary = this.burglaryConfRes.CpiNumber;
        }
      }
      this.payment_confirmation = res;
      this.cs.showLoader = false;
      if (this.paymentConfRes.CpiNumber==null || this.paymentConfRes.CpiNumber==undefined) {
        this.generateCPINumber(this.paymentConfRes.PolicyID, 'fire');
      }
      else {
        this.CpiNumber_fire = this.paymentConfRes.CpiNumber;
      }

      if (res.Message) {
        alert(res.Message);
      } else {
        if (res.PaymentStatus == 'Successful Payment') {
          // console.log("PAyment succ");
          let msg = 'Payment is Successful. Your Transaction ID is ' + res.TransactionID;
          this.toastr.success(msg);
          this.showConfirmationDetails = true;
        } else if (res.PaymentStatus == 'Pending Payment') {
          let msg = 'Payment is not Successful';
          this.toastr.error(msg); this.showConfirmationDetails = false;
        } else if (res.PaymentStatus == 'Failed Payment') {
          if (res.GatewayError != '' || res.GatewayError != null) {
            let msg = 'Payment is Unsuccessful. Message: ' + data.GatewayError;
            this.toastr.error(msg); this.showConfirmationDetails = false;
          } else {
            let msg = 'Payment is Unsuccessful. Please Try again';
            this.toastr.error(msg); this.showConfirmationDetails = false;
          }
        } else {
          let msg = 'Payment is Cancelled. Please Try again';
          this.toastr.error(msg); this.showConfirmationDetails = false;
        }
      }
    });

  }
  downloadPdf(data: any) {
    this.cs.showLoader = true;
    let downloadURL = this.cs.pdfDownload('POLICY', data.EPolicyID)
    this.cs.save(downloadURL, data.ProposalNumber + ".pdf");
    this.cs.showLoader = false;
    this.toastr.success('Your pdf will be downloaded');
  }

  fileSelection(ev: any, typ: any, isMandatory) {
    //console.log("Mantory:" + isMandatory);
    this.imageArray[typ] = ev.target.files[0];
    //ev.target.value = "";
  }
  previewImage(type: any) {
    if (this.imageArray[type] == null || this.imageArray[type] === null) {
      this.toastr.error("Please select image first.")
      return;
    }
    this.previewImageUrl = URL.createObjectURL(this.imageArray[type]);
    if (this.imageArray[type].type != 'application/pdf') {
      (document.getElementById("imageName") as HTMLElement).innerHTML = name + this.imageArray[type].name;
      var element = document.getElementById("previewBtn") as HTMLElement;
      element.style.display = "inline-block";
      var element1 = document.getElementById("uploadDocCard") as HTMLElement;
      element1.style.display = "none";
    }
    else {
      //this.goToLink(this.previewImageUrl)
    }

    //$("#imageName").text(name + this.imageArray[type].name);
    //$(".select-opt-layer-inspectionmode").fadeIn(300);
    // this.currentState="void";
  }
  generateCPINumber(policyId, type) {
    this.cs.showLoader = true;
    this.cs.postUserData('/api/smecorp/GenerateCpiNumber', policyId).then((res: any) => {
      let result = res;
      if (type == 'fire') {
        this.CpiNumber_fire = res.CpiNumber;

      }
      else {
        this.CpiNumber_burglary = res.CpiNumber;
      }
      if ((this.CpiNumber_fire == null || this.CpiNumber_fire == '' || this.CpiNumber_fire == undefined) && type == 'fire') {
        this.toastr.error('CPI number was not generated for ' + type);
      }
      if ((this.CpiNumber_burglary == null || this.CpiNumber_burglary == '' || this.CpiNumber_burglary == undefined) && type == 'burglary') {
        this.toastr.error('CPI number was not generated for ' + type);
      }
      this.cs.showLoader = false;
    })
    // this.generateHtFolderIndex(policyId)
  }

  generateHtFolderIndex(cpinumber) {

    let folderIndexReq = {
      "AppName": "RMTracker",
      "FolderIndex": null,
      "FolderName": cpinumber,
      "searchFolder": false
    }
    this.cs.showLoader = true;
    this.cs.GenerateHtNumber("/api/Utility/DmsSearchCreateFolder", folderIndexReq).then((req: any) => {
      this.cs.showLoader = false;
      if (req != null && req.StatusCode == 1) {
        this.toastr.success('Folder Created Successfully');
        this.checkImageToUpload(req.FolderIndex);
      } else {
        // this.toastr.success('File Upload Falied');
        this.toastr.error(req.StatusDesc);
      }

    }).catch((err) => {
      this.cs.showLoader = false;
      if (err != null &&
        err != undefined) {
        this.toastr.error(err.error);
      }
    });

  }

  /*
   This method selects the image to upload to the 
   server.
   */
  checkImageToUpload(folderindex) {
    this.uploadImageToServer(this.imageArray[0], this.imageArray[0], folderindex);
  }

  /*
  This method is used to call service which will upload
  image in the server.ev parameter is the file and filename is 
  the parameter send to the server.
  */
  uploadImageToServer(ev: any, filename: any, folderindex) {
    this.cs.showLoader = true;
    let docUploadReq = new FormData();
    docUploadReq.append("AppName", "RMTracker");
    docUploadReq.append("FolderIndex", folderindex);
    docUploadReq.append("DocName", filename.name + "_" + new Date().getTime() + ".jpeg");
    docUploadReq.append("file", ev);
    this.cs.uploadImageToDMS("/api/Utility/DmsDocUpload", docUploadReq).then((req: any) => {
      this.cs.showLoader = false;
      if (req.StatusCode == 1) {
        this.toastr.success(req.StatusDesc);
      } else {
        this.toastr.error(req.StatusDesc);
        /// this.openUploadAgainScreen();
      }
    }).catch((err) => {
      this.cs.showLoader = false;
      if (err != null) {
        this.toastr.error(err.error);
      }
    })
  }

  uploadDocuments(type) {
    if (type == 'fire' && this.CpiNumber_fire != undefined) {
      this.generateHtFolderIndex(this.CpiNumber_fire);
    }
    else if (type == 'burglary' && this.CpiNumber_burglary != undefined) {
      this.generateHtFolderIndex(this.CpiNumber_burglary);
    }
    else {
      this.toastr.error('Sorry cannot upload as cpi number not obtained');
    }

    //   let serviceRequest="<Document>"+
    //   "<AppName>RMTracker</AppName>"+
    //   "<FolderName>"+$scope.cpiNumber+"</FolderName>"+
    //   "<DocName>"+(new Date()).getTime()+".png"+"</DocName>"+
    //   "<FileStreamGeneric>"+this.imageArray[0].url+"</FileStreamGeneric>"+
    //  "</Document>";
    //  this.cs.getUserData('/api/Payment/SmeCommonPaymentConfirmationByPID?EPaymentID=' + serviceRequest).then((res: any) => {

    //  })
  }

}
