import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { config } from 'src/app/config.properties';

declare var Razorpay: any;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  commonDataRes: any; proposal_request_data: any; childPageDetails: Window;
  proposal_response_data: any;
  proposal_customer_data: any;

  constructor(public cs: CommonService, public route: Router) { }

  ngOnInit() {
    this.getMotorPayData();
    this.ReceiveMessage = this.ReceiveMessage.bind(this);
    if (!window['postMessage']) {
      alert("oh crap");
    } else {
      if (window.addEventListener) {
        window.addEventListener("message", this.ReceiveMessage, false);
      } else {
        // console.log("onmessage", this.ReceiveMessage);
        // window.attachEvent("onmessage", this.ReceiveMessage);
      }
    }
  }

  getMotorPayData() {
    this.proposal_customer_data=JSON.parse(atob(localStorage.getItem('customerRequest')));
    this.proposal_request_data = JSON.parse(atob(localStorage.getItem('proposalRequest')));
    this.proposal_response_data = JSON.parse(atob(localStorage.getItem('proposalResponse')));
  }

  makePayment1(mode: any, ev: any) {
    this.cs.showLoader = true;
    this.beforeRazorPay().subscribe(() => {
      this.payByRazorPay(this.commonDataRes, mode, 'easyProtect', this.route);
    })
  }

  beforeRazorPay(): Observable<any> {
    return new Observable((data: any) => {
      let policies;
      if (this.proposal_request_data.optingBurglary) {
        policies = this.proposal_response_data.firePolDetails.PolicyId + ':' + this.proposal_response_data.burglaryPolDetails.PolicyId;
      }
      else {
        policies = this.proposal_response_data.firePolDetails.PolicyId;
      }
      let payBody = {
        "TransType": "POLICY_PAYMENT",
        "PaymentMode": "RAZORPAY",
        "UserRole": "AGENT",
        "PaymentAmount": "",
        "PayerType": "Customer",
        "PolicyIDs": policies,
        "ModeID": "",
        "PanNumber": "",
        "OldPaymentID": "",
        "IPAddress": config.ipAddress,
        "ChequeDetails": "",
        "CardDetails": "",
        "GatewayReturnURL": "",
        "BankName": "",
        "PidDetails": ""
      }
      localStorage.setItem('commonPayReq', btoa(JSON.stringify(payBody)));
      this.cs.postData('/api/smecorp/SmeCommonPayment', JSON.stringify(payBody)).subscribe((res: any) => {
        //console.log("Common Pay Response", res);
        this.commonDataRes = res
        localStorage.setItem('payData', btoa(JSON.stringify(res)));
        this.cs.showLoader = false;
        data.next();
      })
    });
  }

  payByRazorPay(res: any, type: any, produtType: any, router: any): Promise<any> {
    return new Promise((resolve) => {
      // console.log("Response", this.commonDataRes);
      // this.cs.showSwapLoader = true;
      var desc = "ICICI Lombard - " + produtType + " Insurance";
      let self = this;
      let options = {
        "description": desc,
        "image": 'https://www.icicilombard.com/mobile/mclaim/images/favicon.ico',
        "currency": 'INR',
        "key": res.PublicKey,
        "order_id": res.RazorOrderID,
        "method": {
          "netbanking": {
            "order": ["ICIC", "HDFC", "SBIN", "UTIB", "IDFB", "IBKL"]
          }
        },
        "prefill": {
          email: this.proposal_customer_data.EmailAddress,
          contact: this.proposal_customer_data.MobileNumber,
          name: this.proposal_request_data.customerName,
          method: type
        },
        "theme": {
          "color": '#E04844',
          "hide_topbar": true
        },
        "handler": function (response: any) {
          //console.log(response);
          let payData = JSON.parse(atob(localStorage.getItem('payData')));
          let params = {
            "iPaymentID": payData.PaymentID,
            "PaymentID": response.razorpay_payment_id,
            "OrderID": response.razorpay_order_id,
            "Signature": response.razorpay_signature
          }

          // console.log("Screen", screen.width, screen.height);
          let width = screen.width / 2;
          let height = screen.height / 2;
          let left = (screen.width - width) / 2;
          let top = (screen.height - height) / 4;

          let childWindow = window.open('#/easy-protect-razorpay-fallback', 'Childwindow', 'status=0,toolbar=0,menubar=0,resizable=0,scrollbars=1,top=' + top + ' ,left=' + left + ',height=' + height + ',width=' + width + '');
          window['child'] = childWindow;
          window['router'] = router;

          // console.log("Child Window 1 ", childWindow);
          self.childPageDetails = childWindow;
          // console.log("Child Window 2 ", self.childPageDetails);
          childWindow.onload = (e) => {
            // console.log("Child Window", e)
            if (childWindow == null || !window['postMessage']) {
              alert("failed");
            } else {
              // childWindow.postMessage({ 'razorPayDetails': params, 'url': config.razorPayFallBackURL + '/PaymentGateway/SmeRazorPayPGIPaymentProcess' }, 'http://localhost:4200/#/easy-protect-payment');
              childWindow.postMessage({ 'razorPayDetails': params, 'url': config.razorPayFallBackURL + '/PaymentGateway/SmeRazorPayPGIPaymentProcess' }, config.paymentURL);
            }
          }
        }
      };
      var rzp1 = new Razorpay(options);
      // console.log(rzp1, this.commonDataRes);
      rzp1.open();

      resolve();
    });
  }

  ReceiveMessage(evt: any) {
    if (evt.origin != config.baseURL.substring(0, config.baseURL.lastIndexOf("/"))) {
      return;
    } else {
      var data = evt.data;
      this.childPageDetails.close();
      if (data.event == "razorpayResponse") {
        if (JSON.parse(localStorage.getItem('planFormURL')) == 'PID') {
          this.route.navigateByUrl('payment', { skipLocationChange: true }).then(() => {
            this.route.navigateByUrl('pid');
            localStorage.setItem('isNewPIDReceivedData', 'true');
            localStorage.setItem('PIDReceiveData', JSON.stringify(data));
          });
        } else {
          this.route.navigateByUrl('easy-protect-payment-confirmation');
          localStorage.setItem('razorPayData', btoa(JSON.stringify(data)));
        }
      }
    }


  }


}
