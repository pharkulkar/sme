import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  proposal_request_data:any;
  proposal_response_data:any;
  proposal_customer_data: any;
  allOccupancyData: any;
  segment_name: any;
  constructor(public router: Router,) { }

  ngOnInit() {
    this.proposal_customer_data=JSON.parse(atob(localStorage.getItem('customerRequest')));
    this.proposal_request_data=JSON.parse(atob(localStorage.getItem('proposalRequest')));
    this.proposal_response_data=JSON.parse(atob(localStorage.getItem('proposalResponse')));
    this.allOccupancyData = JSON.parse(atob(localStorage.getItem('userData')));
    let seg=this.allOccupancyData.epSegmentsList.filter(x => x.name == this.proposal_request_data.OccupancyDesc);
    this.segment_name=seg[0].id;
  }

  goToPayment()
  {
    this.router.navigateByUrl('easy-protect-payment');
  }

}
