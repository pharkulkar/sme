import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pageAuth: any;
  loginForm: FormGroup;

  constructor(public router: Router,
    public cs: CommonService,
    ) { }

  ngOnInit() {
    this.createLoginForm();
  }

  createLoginForm() {
    this.loginForm = new FormGroup({
      userName: new FormControl('IM-545242'),
      passWord: new FormControl('dec12345'), 
    });
  }

  login(form: any) {
    window.localStorage.clear();
    this.cs.showLoader = true;
    this.cs.getHomeAuth('/api/smecorp/SmeCorpMaster', form.value).then((data: any) => {
      let userCredentials = form.value.userName + ":" + form.value.passWord;
      localStorage.setItem('IM_ID',form.value.userName);
      let authToken = "Basic " + btoa(userCredentials);
      this.router.navigateByUrl('easy-protect');
      localStorage.setItem('authToken', JSON.stringify(authToken));
      this.cs.authToken = authToken;
      this.pageAuth = data.MappedProduct;
      localStorage.setItem('userData', btoa(JSON.stringify(data)));
      this.cs.showLoader = false;
    }).catch(err => {
      this.cs.showLoader = false;
    });
  }


}
