import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyProtectComponent } from './easy-protect.component';

describe('EasyProtectComponent', () => {
  let component: EasyProtectComponent;
  let fixture: ComponentFixture<EasyProtectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EasyProtectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyProtectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
