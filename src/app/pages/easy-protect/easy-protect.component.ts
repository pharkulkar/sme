import { Component, OnInit, AfterViewInit, AfterContentChecked, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ModalcontrollerComponent } from 'src/app/modalcontroller/modalcontroller.component';
import { CommonService } from 'src/app/services/common.service';
import * as moment from 'moment';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Z_FILTERED } from 'zlib';
import swal from 'sweetalert';
import { find } from 'rxjs/operators';
declare var $: any;
@Component({
  selector: 'app-easy-protect',
  templateUrl: './easy-protect.component.html',
  styleUrls: ['./easy-protect.component.css']
})
export class EasyProtectComponent implements OnInit {

  easyProtectForm: FormGroup
  easyProtectProposal: FormGroup
  Residential: boolean = false;
  Non_industrial: boolean = false;
  Industrial: boolean = false;
  Storage: boolean = false;
  storageOccupancy = [];
  industrialOccupancy = [];
  nonIndustrialOccupancy = [];
  residentialOccupancy = []; inputLoadingPlan = false;
  allOccupancyData: any = [];
  corrCityId: any;
  buttonID: any; appCityList: any; pinData: any; pinDataPer: any;
  stateMasterHB: any; isKeralaCess = false; isRegWithGSTCHI: boolean = false;
  maxRange: number = 10000000;
  minRange: number = 100000;
  stepRange: number = 1;
  valueRange: any = 5000000;
  isCorraddSame = false; corrCityIdPer: any; isRegWithGST: boolean = false;
  appCityListPer: any; minRiskDate: any; maxRiskDate: any; stateID: any;
  isGSTN: boolean = true; isUIN: boolean = false; iskeralaCess: boolean = false;
  occupancyData: any;
  fireCalculatorData: any;
  zone: any;
  zoneEQRate: any;
  finalRate: any;
  totalPremium: any;
  SIforBuilding: number;
  SIforContents: number;
  SIforStocks: number;
  SIforBurglaryContents: number;
  SIforBurglaryStocks: number;
  bulgaryFinalRate: any;
  isBurglaryRequired: any;
  bulgaryCalculationData: any;
  isBuglary: boolean = false;
  isTerrorismSelected: boolean = false;
  constName: string;
  custType: string;
  gstName: string;
  isLongTermDwelling: boolean = false; isbankHypothecation: boolean = false;
  yearsArray = []
  selectedLongTermYear: any;
  gst: any; gstBuglary: any;
  finalPremimum: any; bulgarytotalRate: any; customerType = 'Individual';
  financierBranch: any;
  bankName: any;
  createCustData: any;
  burglaryBasicPremium: any;
  bulgaryFinalRates: any;
  finalRatemin: any;
  minRate: any;
  MintotalPremium: any;
  finalMinPremium: any;
  mingst: any; isoccupancyDefault: boolean = false; revised_premium: any;
  isFireComponentChanged: boolean = false;
  allOccupancy: any[];

  constructor(public dialog: MatDialog, public cs: CommonService, public router: Router, private toastr: ToastrService) {

  }

  ngOnInit() {
    this.createEasyProtectForm();
    this.createEasyProtectProposalForm();
    this.getStateData();
    this.allOccupancyData = JSON.parse(atob(localStorage.getItem('userData')));
    this.residentialOccupancy = this.allOccupancyData.epSegmentsList.filter(x => x.id == 'Dwellings');
    this.nonIndustrialOccupancy = this.allOccupancyData.epSegmentsList.filter(x => x.id == 'non_industrial');
    this.industrialOccupancy = this.allOccupancyData.epSegmentsList.filter(x => x.id == 'industrial');
    this.storageOccupancy = this.allOccupancyData.epSegmentsList.filter(x => x.id == 'Storage');
    this.allOccupancy = this.residentialOccupancy.concat(this.nonIndustrialOccupancy).concat(this.industrialOccupancy).concat(this.storageOccupancy);
    this.minRiskDate = moment().add(0, 'days').format('YYYY-MM-DD');
    this.maxRiskDate = moment().add(30, 'days').format('YYYY-MM-DD');

    this.easyProtectForm.value.PropertyType == 'Owned';

    for (let i = 3; i <= 20; i++) {
      this.yearsArray.push(i);
    }
    this.clickResidential('dwellings');
    $('#occupancyID').parents('.input-field').addClass('focused');

    // ghjkl;okl
    // $(window).on("load", function () {

    //   var inputs;

    //   inputs = document.getElementsByTagName('input');
    //   for (let index = 0; index < inputs.length; ++index) {
    //     var curr = inputs[index];
    //     if ($(curr).val() != '') {
    //       $(curr).addClass('filled');
    //       $(curr).parents('.form-group').addClass('focused');
    //     }
    //   }

    //   var selectbox = document.getElementsByTagName('select');
    //   for (let index = 0; index < selectbox.length; ++index) {
    //     let curr = selectbox[index];
    //     if ($(curr).val() != '') {
    //       $(curr).addClass('filled');
    //       $(curr).parents('.form-group').addClass('focused');
    //     }
    //   }
    // })
    // $('input').attr('autocomplete', 'off');

    // $('input, select, option').focus(function () {
    //   $(this).parents('.form-group').addClass('focused');
    // });

    // $('input, select, option').blur(function () {
    //   var inputValue = $(this).val();
    //   if (inputValue && $(this).hasClass('hasDatepicker')) {
    //     if (inputValue == "") {
    //       $(this).removeClass('filled');
    //       $(this).parents('.form-group').removeClass('focused');
    //     } else {
    //       $(this).addClass('filled');
    //       $(this).parents('.form-group').addClass('focused');
    //     }
    //   }
    // })

    setTimeout(function () {

      //   $('input, select, option').focus(function () {
      //     $(this).parents('.form-group').addClass('focused');
      // });
      $('.input-field input, select').focus(function () {
        $(this).parents('.input-field').addClass('focused');
      });

      $('.input-field input, select').blur(function () {
        var inputValue = $(this).val();
        if (inputValue == "") {
          $(this).removeClass('filled');
          $(this).parents('.input-field').removeClass('focused');
        } else {
          $(this).addClass('filled');
        }
      })

      //   $(window).on("load", function () {

      //     var inputs;

      //     inputs = document.getElementsByTagName('input');
      //     for (index = 0; index < inputs.length; ++index) {
      //         var curr = inputs[index];
      //         if ($(curr).val() != '') {
      //             $(curr).addClass('filled');
      //             $(curr).parents('.form-group').addClass('focused');
      //         }
      //     }

      //     var selectbox = document.getElementsByTagName('select');
      //     for (var index = 0; index < selectbox.length; ++index) {
      //         var curr1 = selectbox[index];
      //         if ($(curr1).val() != '') {
      //             $(curr1).addClass('filled');
      //             $(curr1).parents('.form-group').addClass('focused');
      //         }
      //     }
      // });

    }, 1000);


  }

  createEasyProtectForm() {
    this.easyProtectForm = new FormGroup({
      Occupancy: new FormControl(),
      DwellingRequired: new FormControl('No'),
      PropertyType: new FormControl('Owned'),
      RiskPincode: new FormControl(),
      CustomerState: new FormControl(''),
      additionalSum: new FormControl(),
      VDValues: new FormControl(),
    })
  }

  createEasyProtectProposalForm() {
    this.easyProtectProposal = new FormGroup(
      {
        insuredName: new FormControl(),
        proposalAddr1: new FormControl(),
        proposalAddr2: new FormControl(),
        proposalPin: new FormControl(),
        proposalState: new FormControl(),
        proposalCity: new FormControl(),

        permAdd1: new FormControl(),
        permAdd2: new FormControl(),
        permPinCode: new FormControl(),
        permState: new FormControl(),
        permCity: new FormControl(),

        riskStartDate: new FormControl(),
        riskEndDate: new FormControl(),
        applEmail: new FormControl(),
        applMobNo: new FormControl(),
        panNo: new FormControl(),
        contactName: new FormControl(),

        fireriskdesc: new FormControl(),
        buglaryriskdesc: new FormControl(),

        constitution: new FormControl(''),
        customerType: new FormControl(''),
        gstRegStatus: new FormControl(''),

        GSTIN: new FormControl(),
        UIN: new FormControl(),

      }
    )
  }

  validatepanCard(val) {
    if (new RegExp('[A-Z]{5}[0-9]{4}[A-Z]{1}').test(val)) {
      return true;
    }
    return false;
  }


  validateMobileLength(e: any) {
    // console.log(e)
    if (e.target.value.length > 10) {
      // this.presentAlert("Mobile No can't be greater than 10 digits");
      alert("Mobile No can't be greater than 10 digits");
      this.easyProtectProposal.patchValue({ 'applMobNo': null });
    } else if (e.target.value.length < 10) {
      // this.presentAlert("Mobile No can't be smaller than 10 digits");
      alert("Mobile No can't be smaller than 10 digits");
      this.easyProtectProposal.patchValue({ 'applMobNo': null });
    }
  }

  bindAddress1(val) {
    this.easyProtectProposal.patchValue({ 'permAdd1': val });
    $('#corresAddone').parents('.input-field').addClass('focused');
  }
  bindAddress2(val) {
    this.easyProtectProposal.patchValue({ 'permAdd2': val });
    $('#correstwo').parents('.input-field').addClass('focused');
  }

  isGSTReg(val) {
    if (val) {
      this.isRegWithGST = true;
      this.easyProtectProposal.get('constitution').setValidators([Validators.required]);
      // this.applicantForm.get('panNo').setValidators([Validators.required]);
      this.easyProtectProposal.get('customerType').setValidators([Validators.required]);
      this.easyProtectProposal.get('gstRegStatus').setValidators([Validators.required]);
      this.easyProtectProposal.get('GSTIN').setValidators([Validators.required]);
      this.easyProtectProposal.get('constitution').updateValueAndValidity();
      // this.applicantForm.get('panNo').updateValueAndValidity();
      this.easyProtectProposal.get('customerType').updateValueAndValidity();
      this.easyProtectProposal.get('gstRegStatus').updateValueAndValidity();
      this.easyProtectProposal.get('GSTIN').updateValueAndValidity();
      setTimeout(() => {
        $('#constitution').parents('.input-field').addClass('focused');
        $('#customerType').parents('.input-field').addClass('focused');
        $('#gstRegStatus').parents('.input-field').addClass('focused');
      }, 100);

    } else {
      this.isRegWithGST = false;
      this.easyProtectProposal.patchValue({ 'GSTIN': "" });
      this.easyProtectProposal.patchValue({ 'UIN': "" });
      this.easyProtectProposal.patchValue({ 'gstRegStatus': "" });
      this.easyProtectProposal.patchValue({ 'customerType': "" });
      // this.applicantForm.patchValue({ 'panNo': "" });
      this.easyProtectProposal.patchValue({ 'constitution': "" });
      this.easyProtectProposal.get('constitution').clearValidators();
      // this.applicantForm.get('panNo').clearValidators();
      this.easyProtectProposal.get('customerType').clearValidators();
      this.easyProtectProposal.get('gstRegStatus').clearValidators();
      this.easyProtectProposal.get('GSTIN').clearValidators();
      this.easyProtectProposal.get('UIN').clearValidators();
      this.easyProtectProposal.get('constitution').updateValueAndValidity();
      // this.applicantForm.get('panNo').updateValueAndValidity();
      this.easyProtectProposal.get('customerType').updateValueAndValidity();
      this.easyProtectProposal.get('gstRegStatus').updateValueAndValidity();
      this.easyProtectProposal.get('GSTIN').updateValueAndValidity();
      this.easyProtectProposal.get('UIN').updateValueAndValidity();
    }
  }

  getConstName(ev: any) {
    if (ev.target.value == "0") { this.constName = "Constitution of Business" }
    else if (ev.target.value == "1") { this.constName = "Non Resident Entity" }
    else if (ev.target.value == "2") { this.constName = "Foreign company registered in India" }
    else if (ev.target.value == "3") { this.constName = "Foreign LLP" }
    else if (ev.target.value == "4") { this.constName = "Government Department" }
    else if (ev.target.value == "5") { this.constName = "Hindu Undivided Family" }
    else if (ev.target.value == "6") { this.constName = "LLP Partnership" }
    else if (ev.target.value == "7") { this.constName = "Local Authorities" }
    else if (ev.target.value == "8") { this.constName = "Partnership" }
    else if (ev.target.value == "9") { this.constName = "Private Limited Company" }
    else if (ev.target.value == "10") { this.constName = "Proprietorship" }
    else { this.constName = "Others" }
  }

  getCustTypeName(ev: any) {
    if (ev.target.value == "0") { this.custType = "Customer Type" }
    else if (ev.target.value == "21") { this.custType = "General" }
    else if (ev.target.value == "22") { this.custType = "EOU/STP/EHTP" }
    else if (ev.target.value == "23") { this.custType = "Government" }
    else if (ev.target.value == "24") { this.custType = "Overseas" }
    else if (ev.target.value == "25") { this.custType = "Related parties" }
    else if (ev.target.value == "26") { this.custType = "SEZ" }
    else { this.custType = "Others" }
  }

  getGSTName(ev: any) {
    if (ev.target.value == "0") { this.gstName = "GST Registration Status" }
    else if (ev.target.value == "41") { this.gstName = "ARN Generated" }
    else if (ev.target.value == "42") { this.gstName = "Provision ID Obtained" }
    else if (ev.target.value == "43") { this.gstName = "To be commenced" }
    else if (ev.target.value == "44") { this.gstName = "Enrolled" }
    else { this.gstName = "Not applicable" }
  }

  radio(val) {
    this.isUIN = val; this.isGSTN = !val;
    if (val == true) {
      this.easyProtectProposal.get('GSTIN').setValidators([Validators.required]);
      this.easyProtectProposal.get('GSTIN').updateValueAndValidity();
      this.easyProtectProposal.patchValue({ 'UIN': "" });
      this.easyProtectProposal.get('UIN').clearValidators();
      this.easyProtectProposal.get('UIN').updateValueAndValidity();
      $('#uin').parents('.input-field').addClass('focused');
    } else {
      this.easyProtectProposal.get('UIN').setValidators([Validators.required]);
      this.easyProtectProposal.get('UIN').updateValueAndValidity();
      this.easyProtectProposal.patchValue({ 'GSTIN': "" });
      this.easyProtectProposal.get('GSTIN').clearValidators();
      this.easyProtectProposal.get('GSTIN').updateValueAndValidity();
      $('#gstin').parents('.input-field').addClass('focused');
    }
  }

  corrFunction(val) {
    if (val) {
      this.isCorraddSame = true;
      this.easyProtectProposal.patchValue({ 'permAdd1': this.easyProtectProposal.value.proposalAddr1 });
      this.easyProtectProposal.patchValue({ 'permAdd2': this.easyProtectProposal.value.proposalAddr2 });
      this.easyProtectProposal.patchValue({ 'permPinCode': this.easyProtectProposal.value.proposalPin });
      this.easyProtectProposal.patchValue({ 'permState': this.easyProtectProposal.value.proposalState });
      this.easyProtectProposal.patchValue({ 'permCity': this.easyProtectProposal.value.proposalCity });
      this.getPincodeDetailsifSame(this.easyProtectProposal.value.proposalPin);
    } else {
      this.isCorraddSame = false;
      this.easyProtectProposal.patchValue({ 'permAdd1': '' });
      this.easyProtectProposal.patchValue({ 'permAdd2': '' });
      this.easyProtectProposal.patchValue({ 'permPinCode': '' });
      this.easyProtectProposal.patchValue({ 'permState': '' });
      this.easyProtectProposal.patchValue({ 'permCity': '' });
    }
  }
  getPincodeDetailsifSame(val) {
    if (val.length == 6) {
      let body = val;
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).toPromise().then((res) => {
        this.pinDataPer = res;
        if (this.pinDataPer.StatusCode == 1) {
          this.appCityListPer = this.pinDataPer.CityList;
          this.corrCityIdPer = this.pinDataPer.CityList[0].CityID;
          this.easyProtectProposal.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
          this.easyProtectProposal.patchValue({ 'permState': this.pinDataPer.StateName });
          let proposalStateCity = { pin: body, city: this.pinDataPer.CityList[0].CityName, state: this.pinDataPer.StateName };
          localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
          this.cs.showLoader = false;
        }
        else {
          this.easyProtectProposal.patchValue({ 'permCity': null });
          this.easyProtectProposal.patchValue({ 'permState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          this.toastr.error(res.StatusMessage);
          this.cs.showLoader = false;
        }
      });
    }
  }

  getPincodeDetailsPer(val: any) {
    if (val.length == 6) {
      let body = val;
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', body).toPromise().then((res) => {
        this.pinDataPer = res;
        if (this.pinDataPer.StatusCode == 1) {
          this.appCityListPer = this.pinDataPer.CityList;
          this.corrCityIdPer = this.pinDataPer.CityList[0].CityID;
          this.easyProtectProposal.patchValue({ 'permCity': this.pinDataPer.CityList[0].CityName });
          this.easyProtectProposal.patchValue({ 'permState': this.pinDataPer.StateName });
          let proposalStateCity = { pin: body, city: this.pinDataPer.CityList[0].CityName, state: this.pinDataPer.StateName };
          localStorage.setItem('PermanentStateCity', JSON.stringify(proposalStateCity));
          this.cs.showLoader = false;
        }
        else {
          this.easyProtectProposal.patchValue({ 'permCity': null });
          this.easyProtectProposal.patchValue({ 'permState': null });
          let proposalStateCity = { pin: body, city: null, state: null };
          this.toastr.error(res.StatusMessage);
          this.cs.showLoader = false;
        }
      });
    }
  }

  changeCityCodePer(ev) {
    this.easyProtectProposal.patchValue({ 'permCity': ev.target.value });
    let cityListForIdPer = this.appCityListPer.filter(city => city.CityName == ev.target.value)
    this.corrCityIdPer = cityListForIdPer[0].CityID;
  }

  clickResidential(data) {
    this.buttonID = data; this.isLongTermDwelling = false;
    this.allOccupancy = this.residentialOccupancy;
    this.Residential = true; this.Non_industrial = false; this.Industrial = false; this.Storage = false;
    this.easyProtectForm.patchValue({ Occupancy: this.residentialOccupancy[0].name })
    document.getElementById(this.buttonID).classList.add("segments-selected");
    document.getElementById('non_industrial').classList.remove("segments-selected");
    document.getElementById('industrial').classList.remove("segments-selected");
    document.getElementById('storage').classList.remove("segments-selected");
  }

  clickNonIndustrial(data) {
    this.buttonID = data;
    this.Residential = false; this.Non_industrial = true; this.Industrial = false; this.Storage = false;
    this.allOccupancy = this.nonIndustrialOccupancy;
    let tempData = this.nonIndustrialOccupancy.find(n => n.name == 'Sports Galleries, Outdoor stadiums.');
    this.easyProtectForm.patchValue({ Occupancy: tempData.name });
    document.getElementById(this.buttonID).classList.add("segments-selected");
    document.getElementById('dwellings').classList.remove("segments-selected");
    document.getElementById('industrial').classList.remove("segments-selected");
    document.getElementById('storage').classList.remove("segments-selected");

  }
  clickIndustrial(data) {
    this.buttonID = data;
    this.Residential = false; 
    this.Non_industrial = false; 
    this.Industrial = true; 
    this.Storage = false;
    this.allOccupancy = this.industrialOccupancy;
    this.easyProtectForm.patchValue({ Occupancy: 'Turpentine and rosin distilleries' });
    document.getElementById(this.buttonID).classList.add("segments-selected");
    document.getElementById('non_industrial').classList.remove("segments-selected");
    document.getElementById('dwellings').classList.remove("segments-selected");
    document.getElementById('storage').classList.remove("segments-selected");
  }
  clickStorage(data) {
    this.buttonID = data;
    this.Residential = false; 
    this.Non_industrial = false; 
    this.Industrial = false; 
    this.Storage = true;
    this.allOccupancy = this.storageOccupancy;
    let tempData = this.storageOccupancy.find(n => n.name == 'Storage of hazardous Goods listed in Category II subject to warranty that goods listed in Category III, Coir waste, Coir fibre and Caddies are not stored therein. - Storage in godown or warehouse');
    this.easyProtectForm.patchValue({ Occupancy: tempData.name });
    document.getElementById(this.buttonID).classList.add("segments-selected");
    document.getElementById('non_industrial').classList.remove("segments-selected");
    document.getElementById('industrial').classList.remove("segments-selected");
    document.getElementById('dwellings').classList.remove("segments-selected");
  }
  openDialog() {
    let allData = this.residentialOccupancy.concat(this.nonIndustrialOccupancy).concat(this.industrialOccupancy).concat(this.storageOccupancy);
    const dialogRef = this.dialog.open(ModalcontrollerComponent, {
      data: {
        data: this.buttonID == 'dwellings' ? this.residentialOccupancy : this.buttonID == 'non_industrial' ?
          this.nonIndustrialOccupancy : this.buttonID == 'industrial' ? this.industrialOccupancy : this.buttonID == 'storage' ? this.storageOccupancy : allData
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.buttonID == undefined) {
        let temp = allData.filter(x => x.name == result);
        this.buttonID = temp[0].id;
        if (temp[0].id == 'Dwellings') {
          this.buttonID = 'dwellings'; this.Residential = true;
        }
        else if (temp[0].id == 'non_industrial') {
          this.buttonID = 'non_industrial';
        }
        else if (temp[0].id == 'industrial') {
          this.buttonID = 'industrial';
        }
        else if (temp[0].id == 'Storage') {
          this.buttonID = 'storage';
        }
      }

      document.getElementById(this.buttonID).classList.add("segments-selected");
      this.easyProtectForm.patchValue({ Occupancy: result })
    });
  }

  getPincodeDetails(val: any) {

    if (val.length == 6) {
      this.cs.showLoader = true;
      this.cs.postWithParams('/api/rtolist/GetStatesCityByPin', val).subscribe((res) => {
        this.pinData = res;
        if (this.pinData.StatusCode == 1) {
          this.appCityList = this.pinData.CityList;
          this.corrCityId = this.pinData.CityList[0].CityID;
          this.isCorraddSame = true;
          this.easyProtectProposal.patchValue({ 'proposalPin': this.pinData.Pincode });
          this.easyProtectForm.patchValue({ 'CustomerState': this.pinData.StateName })
          this.easyProtectProposal.patchValue({ 'proposalState': this.pinData.StateName });
          this.easyProtectProposal.patchValue({ 'proposalCity': this.pinData.CityList[0].CityName });
          this.easyProtectProposal.patchValue({ 'permPinCode': this.pinData.Pincode });
          let proposalStateCity = { pin: val, city: this.pinData.CityList[0].CityName, state: this.pinData.StateName };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          this.cs.showLoader = false;
          $('#custState').parents('.input-field').addClass('focused');
          $('#propCity').parents('.input-field').addClass('focused');
          $('#corresPincode').parents('.input-field').addClass('focused');
          this.getPincodeDetailsPer(this.pinData.Pincode);
        }
        else {
          let proposalStateCity = { pin: val, city: null, state: null };
          localStorage.setItem('CoresspondentStateCity', JSON.stringify(proposalStateCity));
          this.toastr.error(res.StatusMessage);
          this.easyProtectForm.patchValue({ 'RiskPincode': null });
          this.cs.showLoader = false;
        }
      });
    }
  }

  getStateData() {
    this.cs.postWithParams('/api/MotorMaster/GetAllStates', '').subscribe(res => {
      this.stateMasterHB = res;
      this.stateMasterHB = this.stateMasterHB, 'StateName';
      // console.log(this.stateMasterHB, "State Data")
      // let i:any;
      // for(i=0; i< this.stateMasterHB.length;i++)
      // {
      //   console.log(this.stateMasterHB[i].StateId,"State ID")
      // }

      // this.stateID = this.stateMasterHB[i].StateId;

      let statename = this.stateMasterHB.find(x => x.StateId == 55).StateName;
      // console.log(statename, "state")
      // this.quoteFormHB.patchValue({ 'state': statename });
      // this.quoteForm.patchValue({ 'state': statename });
      // this.selectedState = 55;
    });
  }

  getState(e: any) {
    // console.log(e.target.value);
    if (e.target.value == 'Kerala' || e.target.value == 'KERALA' || e.target.value == 'kerala') {
      this.isKeralaCess = true;
    }
    else {
      this.isKeralaCess = false; this.isRegWithGSTCHI = false;
    }
  }

  findPincodeZone(ev) {
    if (ev.length == 6) {
      this.allOccupancyData.zoneMaster.filter(x => {
        if (x.PINCODE == ev) {
          this.zone = x.ZONE.replace(/Zone_/g, '').trim();
        }
      })
    }
  }

  premiumLessThanHundred(basePremium: any): Promise<any> {
    return new Promise((resolve) => {
      if (this.isBuglary) {
        this.finalRate = parseFloat(this.bulgaryCalculationData.rsmd) + parseFloat(this.bulgaryCalculationData.BASIC)
        basePremium = (this.valueRange * this.finalRate) / 1000;
        let diff: any = 100 - basePremium;
        let diff1: any = ((diff / this.valueRange) * 1000);
        var new_basic_rate: any = parseFloat(this.bulgaryCalculationData.BASIC) + parseFloat(diff1);// need to fix value of diff
        var new_final_rate = parseFloat(this.bulgaryCalculationData.rsmd) + parseFloat(new_basic_rate);
        this.totalPremium = (parseFloat(this.valueRange) * new_final_rate) / 1000;
        resolve();
      }
      else {
        basePremium = (this.valueRange * this.finalRate) / 1000;
        let diff: any = 100 - basePremium;
        let diff1: any = (diff / this.valueRange * 1000);
        var new_flexa_rate: any = parseFloat(this.fireCalculatorData.FLEXA) + parseFloat(diff1);// need to fix value of diff
        var new_final_rate = parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.fireCalculatorData.STFI) + parseFloat(this.fireCalculatorData.EQ) + parseFloat(new_flexa_rate);
        //  if(this.isTerrorismSelected == true){
        //    new_final_rate = new_final_rate + parseFloat(this.occupancyData[0]['TERRORISM_RATE']);
        //  }
        this.totalPremium = (parseFloat(this.valueRange) * new_final_rate) / 1000;
        resolve();
      }
    })
  }
  @ViewChild('divToScroll', { static: false }) divToScroll: any
  validateQuote() {
    if (this.buttonID == null || this.buttonID == undefined || this.buttonID == "") { this.toastr.error("Select Segment") }
    else if (this.easyProtectForm.value.Occupancy == null || this.easyProtectForm.value.Occupancy == undefined || this.easyProtectForm.value.Occupancy == '') { this.toastr.error("Search Occupancy") }
    else if (this.easyProtectForm.value.PropertyType == null || this.easyProtectForm.value.PropertyType == undefined || this.easyProtectForm.value.PropertyType == '') { this.toastr.error("Select Property Type") }
    else if (this.easyProtectForm.value.RiskPincode == null || this.easyProtectForm.value.RiskPincode == undefined || this.easyProtectForm.value.RiskPincode == '') { this.toastr.error("Enter Risk Location Pincode") }
    else if (this.easyProtectForm.value.CustomerState == null || this.easyProtectForm.value.CustomerState == undefined || this.easyProtectForm.value.CustomerState == '') { this.toastr.error("Select State") }
    else if (this.isLongTermDwelling == true && (this.selectedLongTermYear == undefined || this.selectedLongTermYear == null || this.selectedLongTermYear == '')) {
      this.toastr.error("Select a year");
    }
    else {
      // document.getElementById('segDetails').classList.add("disableDiv");
      // document.getElementById('segDetails').classList.remove("show");
      // document.getElementById('linkSegment').classList.add("collapsed");
      this.quickQouteCalculation();
      document.getElementById('qQuote').classList.add("show");
      this.divToScroll.nativeElement.scrollIntoView() - 300;
      //document.getElementById('propDetails').classList.add("show");

    }
  }

  validateProposal() {
    if (this.easyProtectProposal.value.insuredName == null || this.easyProtectProposal.value.insuredName == undefined || this.easyProtectProposal.value.insuredName == '') { this.toastr.error("Enter Insured Name") }
    else if (this.easyProtectProposal.value.proposalAddr1 == null || this.easyProtectProposal.value.proposalAddr1 == undefined || this.easyProtectProposal.value.proposalAddr1 == '') { this.toastr.error("Please Enter Risk Location Address Line 1") }
    else if (this.easyProtectProposal.value.proposalAddr2 == null || this.easyProtectProposal.value.proposalAddr2 == undefined || this.easyProtectProposal.value.proposalAddr2 == '') { this.toastr.error("Please Enter Risk Location Address Line 2") }
    else if (this.isCorraddSame == false) {
      if (this.easyProtectProposal.value.permAdd1 == null || this.easyProtectProposal.value.permAdd1 == undefined || this.easyProtectProposal.value.permAdd1 == '') { this.toastr.error("Please Enter Customer Address Line 1") }
      else if (this.easyProtectProposal.value.permAdd2 == null || this.easyProtectProposal.value.permAdd2 == undefined || this.easyProtectProposal.value.permAdd2 == '') { this.toastr.error("Please Enter Customer Address Line 2") }
      else if (this.easyProtectProposal.value.permPinCode == null || this.easyProtectProposal.value.permPinCode == undefined || this.easyProtectProposal.value.permPinCode == '') { this.toastr.error("Please Enter Customer Pincode") }
    }
    else if (this.easyProtectProposal.value.riskStartDate == null || this.easyProtectProposal.value.riskStartDate == undefined || this.easyProtectProposal.value.riskStartDate == '') { this.toastr.error("Please Enter Risk Start Date") }
    else if (this.easyProtectProposal.value.fireriskdesc == null || this.easyProtectProposal.value.fireriskdesc == undefined || this.easyProtectProposal.value.fireriskdesc == '') { this.toastr.error("Please Enter Risk Description for Fire") }
    else if (this.isBuglary == true && (this.easyProtectProposal.value.buglaryriskdesc == null || this.easyProtectProposal.value.buglaryriskdesc == null || this.easyProtectProposal.value.buglaryriskdesc == null)) {
      this.toastr.error("Please Enter Risk Description for Burglary")
    }
    else if (this.easyProtectProposal.value.applEmail == null || this.easyProtectProposal.value.applEmail == undefined || this.easyProtectProposal.value.applEmail == '') { this.toastr.error("Please Enter Email ID") }
    else if (this.easyProtectProposal.value.applMobNo == null || this.easyProtectProposal.value.applMobNo == undefined || this.easyProtectProposal.value.applMobNo == '') { this.toastr.error("Please Enter Mobile Number") }
    else if (this.easyProtectProposal.value.panNo != null && this.easyProtectProposal.value.panNo != undefined && this.easyProtectProposal.value.panNo != '' && !this.validatepanCard(this.easyProtectProposal.value.panNo)) {
      this.toastr.error("Please Enter Valid Pan Number");
    }
    else if (this.easyProtectProposal.value.contactName == null || this.easyProtectProposal.value.contactName == undefined || this.easyProtectProposal.value.contactName == '') { this.toastr.error("Please Enter Contact Person Name") }
    else if (this.isRegWithGST == true) {
      if (this.easyProtectProposal.value.constitution == null || this.easyProtectProposal.value.constitution == undefined || this.easyProtectProposal.value.constitution == '') { this.toastr.error("Please Select Constitution of Business") }
      else if (this.easyProtectProposal.value.customerType == null || this.easyProtectProposal.value.customerType == undefined || this.easyProtectProposal.value.customerType == '') { this.toastr.error("Please Select Customer Type") }
      else if (this.easyProtectProposal.value.gstRegStatus == null || this.easyProtectProposal.value.gstRegStatus == undefined || this.easyProtectProposal.value.gstRegStatus == '') { this.toastr.error("Please Select GST Registration Status") }
      else if (this.isGSTN == true) { if (this.easyProtectProposal.value.GSTIN == null || this.easyProtectProposal.value.GSTIN == undefined || this.easyProtectProposal.value.GSTIN == '') { this.toastr.error("Please Enter GSTIN Number") } }
      else if (this.isUIN == true) { if (this.easyProtectProposal.value.UIN == null || this.easyProtectProposal.value.UIN == undefined || this.easyProtectProposal.value.UIN == '') { this.toastr.error("Please Enter UIN Number") } }
    }
    else {
      swal('It is assumed that Construction is RCC and there is no Basement Risk and claim experience for last 3 years is nil. Please click on Ok to proceed and Cancel to discard the Proposal', {
        buttons: ["Cancel", true],
      }).then((willDelete) => {
        if (willDelete) {
          document.getElementById('propDetails').classList.remove("show");
          document.getElementById('propDetaillink').classList.add("collapsed");
          // document.getElementById('propDetails').classList.add("disableDiv");
          document.getElementById('addInfo').classList.add("show");
          document.getElementById('addLink').classList.remove("collapsed");
          document.getElementById('addInfo').classList.add("show");

          // document.getElementById('addInfo').style.display = 'block'
        }
      });
    }
  }


  quickQouteCalculation() {
    this.occupancyData = this.allOccupancyData.epSegmentsList.filter(x => x.name == this.easyProtectForm.value.Occupancy);
    this.fireCalculatorData = this.allOccupancyData.fireCalculatorData[this.buttonID];
    this.allOccupancyData.zoneEQRateMaster.filter(x => {
      if (x.SegmentType.includes(this.buttonID.toUpperCase()) && x.ZoneName == this.zone)
        this.zoneEQRate = x.EQRate;
    }
    )
    // this.maxRange = this.occupancyData[0]['MAX_SI_POLICY']

    // Minimum Calculation

    // console.log(this.finalRatemin, "Minimum Final Rate");
    if (this.easyProtectForm.value.Occupancy == 'Dwellings') { this.zoneEQRate = 0.05; }

    if (this.easyProtectForm.value.Occupancy == "Dwellings" ||
      this.easyProtectForm.value.Occupancy == "Sports Galleries, Outdoor stadiums." ||
      this.easyProtectForm.value.Occupancy == "Turpentine and rosin distilleries" ||
      this.easyProtectForm.value.Occupancy == "Storage of hazardous Goods listed in Category II subject to warranty that goods listed in Category III, Coir waste, Coir fibre and Caddies are not stored therein. - Storage in godown or warehouse"
    ) {
      this.finalRate = parseFloat(this.zoneEQRate) + parseFloat(this.fireCalculatorData.STFI) + parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.fireCalculatorData.FLEXA);
      this.isoccupancyDefault = false;
    } else {
      // Minimum rate calculation //
      this.minRate = this.occupancyData[0]['MIN_RATE']
      this.finalRatemin = parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.fireCalculatorData.STFI) + parseFloat(this.zoneEQRate) + parseFloat(this.minRate);
      // Minimum total
      let MintotalPremium: any = Math.round((this.finalRatemin * this.valueRange) / 1000)
      this.MintotalPremium = parseInt(MintotalPremium);
      // Minimum Final Premium
      let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
      if (selectedStateId == 62) {
        this.mingst = this.MintotalPremium * 0.19;
      }
      else {
        this.mingst = this.MintotalPremium * 0.18;
      }
      this.finalMinPremium = this.MintotalPremium + parseFloat(this.mingst);
      // Minimum rate calculation ends //
      this.finalRate = parseFloat(this.zoneEQRate) + parseFloat(this.fireCalculatorData.STFI) + parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.occupancyData[0]['DEFAULT_RATE']);
      this.isoccupancyDefault = true;
    }
    if (this.isTerrorismSelected) {
      this.finalRate = this.finalRate + parseFloat(this.occupancyData[0]['TERRORISM_RATE'])
    }

    let totalPremium: any = Math.round((this.finalRate * this.valueRange) / 1000)
    this.totalPremium = parseInt(totalPremium);


    // console.log(this.MintotalPremium, "Minimum Basic premium")

    // Calculation for long term dwelling changing rates and bsiac premium
    if (this.isLongTermDwelling && this.occupancyData[0]['name'] == "Dwellings") {
      this.totalPremium = this.totalPremium * this.selectedLongTermYear;
      this.fireCalculatorData.rsmd = parseFloat(this.fireCalculatorData.rsmd) * parseInt(this.selectedLongTermYear);
      this.fireCalculatorData.STFI = parseFloat(this.fireCalculatorData.STFI) * parseInt(this.selectedLongTermYear);
      this.zoneEQRate = parseFloat(this.zoneEQRate) * parseInt(this.selectedLongTermYear);
      this.fireCalculatorData.FLEXA = parseFloat(this.fireCalculatorData.FLEXA) * parseInt(this.selectedLongTermYear);
    }
    // Calculation to alter basic premium if total premium goes below hundred
    if (this.totalPremium < 100) {
      if (this.valueRange >= 100000 && this.valueRange <= 10000000) {
        let total = this.premiumLessThanHundred(this.totalPremium);
      }
      else {
        this.totalPremium = 0; this.finalPremimum = 0;
      }
    }
    // Calculation for basic,GST and total premium
    let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
    if (selectedStateId == 62) {
      this.gst = this.totalPremium * 0.19;
    }
    else {
      this.gst = this.totalPremium * 0.18;
    }
    this.finalPremimum = this.totalPremium + parseFloat(this.gst);
    // console.log(this.gst, "GST amount")
    // console.log(this.finalMinPremium, "Minimum Total premium")
  }

  bulgaryCalculation(val) {

    this.bulgaryCalculationData = this.allOccupancyData.burglaryCalculatorData[this.buttonID];
    let bulgaryrsmd = this.bulgaryCalculationData.rsmd;
    let bulgaryBASIC = this.bulgaryCalculationData.BASIC;
    let bulgaryFinalRate = parseFloat(bulgaryrsmd) + parseFloat(bulgaryBASIC)
    this.bulgaryFinalRate = this.valueRange * bulgaryFinalRate / 1000;
    this.isBuglary = val;
    let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
    if (selectedStateId == 62) {
      this.gstBuglary = this.bulgaryFinalRate * 0.19;
    }
    else {
      this.gstBuglary = this.bulgaryFinalRate * 0.18;
    }
    this.bulgarytotalRate = this.bulgaryFinalRate + parseFloat(this.gstBuglary);
    setTimeout(() => {
     // $('#custState').parents('.input-field').addClass('focused');
    }, 100);
 
  }



  clickToProceed() {
    let totalSI;
    if (this.isFireComponentChanged) {
      totalSI = this.SIforBuilding + this.SIforContents + this.SIforStocks;
    }
    else {
      if (this.easyProtectForm.value.PropertyType == 'Owned') {
        if (this.isBuglary) {
          switch (this.buttonID) {
            case 'dwellings':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'non_industrial':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'industrial':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'storage':
              this.SIforBuilding = 0;
              this.SIforContents = 0;
              this.SIforStocks = this.valueRange;
              break;
            default:
              break;
          }
          this.SIforBurglaryContents = this.SIforContents;
          this.SIforBurglaryStocks = this.SIforStocks;
          this.bulgaryCalculationData = this.allOccupancyData.burglaryCalculatorData[this.buttonID];
          let bulgaryrsmd = this.bulgaryCalculationData.rsmd;
          let bulgaryBASIC = this.bulgaryCalculationData.BASIC;
          this.burglaryBasicPremium = this.SIforBurglaryContents + this.SIforBurglaryStocks;
          this.bulgaryFinalRates = parseFloat(bulgaryrsmd) + parseFloat(bulgaryBASIC)
          this.bulgaryFinalRate = this.burglaryBasicPremium * this.bulgaryFinalRates / 1000;
          let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
          if (selectedStateId == 62) {
            this.gstBuglary = this.bulgaryFinalRate * 0.19;
          }
          else {
            this.gstBuglary = this.bulgaryFinalRate * 0.18;
          }
          this.bulgarytotalRate = this.bulgaryFinalRate + parseFloat(this.gstBuglary);
        }
        else {
          switch (this.buttonID) {
            case 'dwellings':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'non_industrial':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'industrial':
              this.SIforBuilding = Math.round(this.valueRange * 70 / 100);
              this.SIforContents = Math.round(this.valueRange * 30 / 100);
              this.SIforStocks = 0;
              break;
            case 'storage':
              this.SIforBuilding = 0;
              this.SIforContents = 0;
              this.SIforStocks = this.valueRange;
              break;
            default:
              break;
          }
        }
      }
      else {
        if (this.isBuglary) {
          switch (this.buttonID) {
            case 'dwellings':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'non_industrial':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'industrial':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'storage':
              this.SIforBuilding = 0;
              this.SIforContents = 0;
              this.SIforStocks = this.valueRange;
              break;
            default:
              break;

          }
          this.SIforBurglaryContents = this.SIforContents;
          this.SIforBurglaryStocks = this.SIforStocks;
        }
        else {
          switch (this.buttonID) {
            case 'dwellings':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'non_industrial':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'industrial':
              this.SIforBuilding = Math.round(this.valueRange * 50 / 100);
              this.SIforContents = Math.round(this.valueRange * 50 / 100);
              this.SIforStocks = 0;
              break;
            case 'storage':
              this.SIforBuilding = 0;
              this.SIforContents = 0;
              this.SIforStocks = this.valueRange;
              break;
            default:
              break;
          }
        }
      }
      totalSI = this.SIforBuilding + this.SIforContents + this.SIforStocks;
    }
    if (this.valueRange >= 100000 && this.valueRange <= 10000000) {
      if (totalSI == this.valueRange) {
        document.getElementById('quotelink').classList.remove("card-link");
        document.getElementById('quotelink').classList.add("card-link");
        document.getElementById('quotelink').classList.add("collapsed");
        // document.getElementById('qQuote').classList.add("disableDiv");
        document.getElementById('qQuote').classList.remove("show");
        document.getElementById('propDetaillink').classList.remove("collapsed")
        document.getElementById('propDetails').classList.add("show");
      } else {
        this.toastr.error("Values entered should be equal to the sum insured selected.");
      }

    }
    else {
      this.toastr.error("Sum Insured cannot be less than 1 Lac. and greater than 1 Cr.");
    }

  }
  bindRevisedPremium() {
    this.revised_premium = this.finalPremimum;
  }
  getTotalSI() {
    this.isFireComponentChanged = true;
  }
  caluclateRevisedPremium() {
    if (this.isoccupancyDefault) {
      this.changePremiumDefalut();
    }
    else {
      this.changePremiumNonDefault();
    }
  }
  changePremiumNonDefault() {
    if(this.finalMinPremium==undefined || this.finalMinPremium==null){
      this.finalMinPremium=0;
    }
    if (parseInt(this.finalPremimum) >= parseInt(this.finalMinPremium)) {
      if (parseInt(this.revised_premium) > 100000) {
        alert("max premium limit Rs.1,00,000");
      }
      else if (parseInt(this.revised_premium) < parseInt(this.finalMinPremium)) {
        alert("min premium limit Rs. " + this.finalMinPremium);
      }
      else {
        let diff = parseFloat(this.revised_premium) - parseFloat(this.finalPremimum);
        let diff1 = diff * 100 / 118; let diff2 = diff * 100 / 119;
        var new_basic_fire = parseFloat(this.totalPremium) + diff1;
        let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
        if (selectedStateId == 62) {
          new_basic_fire = parseFloat(this.totalPremium) + diff2;
        }
        else {
          new_basic_fire = parseFloat(this.totalPremium) + diff1;
        }

        let new_gst_on_file = new_basic_fire * 18 / 100;
        if (selectedStateId == 62) {
          new_gst_on_file = new_basic_fire * 19 / 100;
        } else {
          new_gst_on_file = new_basic_fire * 18 / 100;
        }

        this.finalPremimum = this.revised_premium;
        this.totalPremium = new_basic_fire.toFixed(2);
        this.gst = new_gst_on_file.toFixed(2);



        let new_flexa_rate = parseFloat(this.fireCalculatorData.FLEXA) + (diff / this.valueRange) * 1000 * 100 / 118;

        if (selectedStateId == 62) {
          new_flexa_rate = parseFloat(this.fireCalculatorData.Fire_FLEXA) + (diff / this.valueRange) * 1000 * 100 / 119;
        } else {
          new_flexa_rate = parseFloat(this.fireCalculatorData.Fire_FLEXA) + (diff / this.valueRange) * 1000 * 100 / 118;
        }

        this.fireCalculatorData.Fire_FLEXA = new_flexa_rate.toFixed(4);

        let revised_final_rate = parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.fireCalculatorData.Fire_STFI) + parseFloat(this.zoneEQRate) + new_flexa_rate;

        if (this.isTerrorismSelected) {
          revised_final_rate = revised_final_rate + parseFloat(this.occupancyData[0]['TERRORISM_RATE']);
        }
        this.finalRate = revised_final_rate.toFixed(4).toString();
      }
    }
    else if (parseInt(this.finalPremimum) < parseInt(this.finalMinPremium)) {
      alert("min premium limit Rs. " + this.finalMinPremium);
      // epUserSelectionData.fireTotalPremium = firprm; //Math.ceil(premium.totalPremium);
      // epUserSelectionData.fireBasicPremium = firbprm; //Math.ceil(premium.basePremium);
      // epUserSelectionData.fireGSTAmount = firogst; // Math.ceil(premium.GST);

    } else {
      // $("#firePremium").text(firprm);

      // epUserSelectionData.fireTotalPremium = firprm; //Math.ceil(premium.totalPremium);
      // epUserSelectionData.fireBasicPremium = firbprm; //Math.ceil(premium.basePremium);
      // epUserSelectionData.fireGSTAmount = firogst; // Math.ceil(premium.GST);
    }
    $('#revPremModal').modal('hide');
  }
  changePremiumDefalut() {
    if (parseInt(this.finalPremimum) >= parseInt(this.finalMinPremium)) {
      if (parseInt(this.revised_premium) > 100000) {
        alert("max premium limit Rs.1,00,000");
      }
      else if (parseInt(this.revised_premium) < parseInt(this.finalMinPremium)) {
        alert("min premium limit Rs. " + this.finalMinPremium);
      }
      else {
        let diff = parseFloat(this.revised_premium) - parseFloat(this.finalPremimum);
        let diff1 = diff * 100 / 118; let diff2 = diff * 100 / 119;
        var new_basic_fire = parseFloat(this.totalPremium) + diff1;
        let selectedStateId = this.stateMasterHB.filter(x => x.StateName == this.easyProtectForm.value.CustomerState).StateId;
        if (selectedStateId == 62) {
          new_basic_fire = parseFloat(this.totalPremium) + diff2;
        } else {
          new_basic_fire = parseFloat(this.totalPremium) + diff1;
          let new_gst_on_file = new_basic_fire * 18 / 100;
          if (selectedStateId == 62) {
            new_gst_on_file = new_basic_fire * 19 / 100;
          } else {
            new_gst_on_file = new_basic_fire * 18 / 100;
          }
          this.finalPremimum = this.revised_premium;
          this.totalPremium = new_basic_fire.toFixed(2);
          this.gst = new_gst_on_file.toFixed(2);

          let new_flexa_rate = parseFloat(this.fireCalculatorData.FLEXA) + (diff / this.valueRange) * 1000 * 100 / 118;


          if (selectedStateId == 62) {
            new_flexa_rate = parseFloat(this.fireCalculatorData.Fire_FLEXA) + (diff / this.valueRange) * 1000 * 100 / 119;
          } else {
            new_flexa_rate = parseFloat(this.fireCalculatorData.Fire_FLEXA) + (diff / this.valueRange) * 1000 * 100 / 118;
          }

          this.fireCalculatorData.Fire_FLEXA = new_flexa_rate.toFixed(4);
          let revised_final_rate = parseFloat(this.fireCalculatorData.rsmd) + parseFloat(this.fireCalculatorData.Fire_STFI) + parseFloat(this.zoneEQRate) + new_flexa_rate;
          if (this.isTerrorismSelected) {
            revised_final_rate = revised_final_rate + parseFloat(this.occupancyData[0]['TERRORISM_RATE']);
          }
          this.finalRate = revised_final_rate.toFixed(4).toString();
        }
      }
    } else if (parseInt(this.finalPremimum) < parseInt(this.finalMinPremium)) {
      alert("min premium limit Rs. " + this.finalMinPremium);
      // epUserSelectionData.fireTotalPremium = firprm; //Math.ceil(premium.totalPremium);
      // epUserSelectionData.fireBasicPremium = firbprm; //Math.ceil(premium.basePremium);
      // epUserSelectionData.fireGSTAmount = firogst; // Math.ceil(premium.GST);

    } else {
      // $("#firePremium").text(firprm);

      // epUserSelectionData.fireTotalPremium = firprm; //Math.ceil(premium.totalPremium);
      // epUserSelectionData.fireBasicPremium = firbprm; //Math.ceil(premium.basePremium);
      // epUserSelectionData.fireGSTAmount = firogst; // Math.ceil(premium.GST);
    }
  }

  getTerrorismOption(val) {
    this.isTerrorismSelected = val;
    this.quickQouteCalculation();
  }

  getLongTermDwelling(val) {
    this.isLongTermDwelling = val;
    if (val) {
      setTimeout(() => {
        $('#longTremDwell').parents('.input-field').addClass('focused');
      }, 100);

    }
  }
  getSelectedLongTermYear(val) {
    this.selectedLongTermYear = val;
    this.quickQouteCalculation();
  }
  getRiskStartDate(val) {
    if (this.isLongTermDwelling) {
      let riskEndDate = moment(val).add('years', this.selectedLongTermYear).format('DD/MM/YYYY');
      this.easyProtectProposal.patchValue({ 'riskEndDate': riskEndDate });
      $('#riskEndDate').parents('.input-field').addClass('focused');
    }
    else {
      let riskEndDate = moment(val).add('years', 1).format('DD/MM/YYYY');
      this.easyProtectProposal.patchValue({ 'riskEndDate': riskEndDate });
      $('#riskEndDate').parents('.input-field').addClass('focused');
    }

  }
  getCustomerType(val) {
    this.customerType = val;
  }
  getBankHypothecation(val) {
    this.isbankHypothecation = val;
    if (val == 'true') {
      this.financierBranch = "JAMNAGAR"
    }
    else {
      this.financierBranch = "NA";;
      this.bankName = "NA";
    }
  }
  getBankName(val) {
    this.bankName = val;
  }

  generateCustomer(): Promise<any> {
    let postdata;
    let state_id, city_id, perm_state_id, perm_city_id, pan_no;
    city_id = this.appCityList.find(c => c.CityName == this.easyProtectProposal.value.proposalCity).CityID;
    state_id = this.stateMasterHB.find(c => c.StateName == this.easyProtectProposal.value.proposalState).StateId;
    perm_state_id = this.stateMasterHB.find(c => c.StateName == this.easyProtectProposal.value.permState).StateId
    perm_city_id = this.appCityListPer.find(c => c.CityName == this.easyProtectProposal.value.proposalCity).CityID;
    if (this.easyProtectProposal.value.applMobNo == undefined || this.easyProtectProposal.value.applMobNo == '' || this.easyProtectProposal.value.applMobNo == null) {
      pan_no = '';
    }
    else {
      pan_no = this.easyProtectProposal.value.panNo;
    }

    return new Promise((resolve) => {
      postdata = {
        "CustomerID": 0,
        "OwnerType": this.easyProtectForm.value.PropertyType,
        "HasAddressChanged": "true",
        "SetAsDefault": true,
        "TitleText": "Mr",
        "TitleValue": "1",
        "Name": this.easyProtectProposal.value.insuredName,
        "DateOfBirth": "03-Aug-1995",
        "MaritalStatusValue": "-1",
        "MaritalStatusText": "",
        "OccupationValue": "-1",
        "OccupationText": "",
        "AnnualIncomeValue": "-1",
        "AnnualIncomeText": "",
        "IdentityProofValue": "-1",
        "IdentityProofText": "",
        "IdentityNumber": "",
        "PresentAddrLine1": this.easyProtectProposal.value.proposalAddr1,
        "PresentAddrLine2": this.easyProtectProposal.value.proposalAddr1,
        "PresentAddrLandmark": "",
        "PresentAddrCityValue": city_id,
        "PresentAddrCityText": this.easyProtectProposal.value.proposalCity,
        "PresentAddrStateValue": state_id,
        "PresentAddrStateText": this.easyProtectProposal.value.proposalState,
        "PresentAddrPincodeValue": "",
        "PresentAddrPincodeText": this.easyProtectProposal.value.proposalPin,
        "EmailAddress": this.easyProtectProposal.value.applEmail,
        "MobileNumber": this.easyProtectProposal.value.applMobNo,
        "LandlineNumber": "",
        "EmailAlternate": this.easyProtectProposal.value.applEmail,
        "MobileAlternate": this.easyProtectProposal.value.applMobNo,
        "PANNumber": pan_no,
        "isPermanentAsPresent": this.isCorraddSame,
        "PermanentAddrLine1": this.easyProtectProposal.value.permAdd1,
        "PermanentAddrLine2": this.easyProtectProposal.value.permAdd1,
        "PermanentAddrLandmark": "",
        "PermanentAddrCityValue": perm_city_id,
        "PermanentAddrCityText": this.easyProtectProposal.value.permCity,
        "PermanentAddrStateValue": perm_state_id,
        "PermanentAddrStateText": this.easyProtectProposal.value.permState,
        "PermanentAddrPincodeValue": "",
        "PermanentAddrPincodeText": this.easyProtectProposal.value.permPinCode,
        "isCorpCustomer": true,
        "ContactPersonName": this.easyProtectProposal.value.contactName
      }
      if (this.isRegWithGST) {
        let gstDetails = {
          "GSTIN_NO": this.easyProtectProposal.value.GSTIN,
          "CONSTITUTION_OF_BUSINESS": this.easyProtectProposal.value.constitution,
          "CONSTITUTION_OF_BUSINESS_TEXT": this.constName,
          "CUSTOMER_TYPE": this.easyProtectProposal.value.customerType,
          "CUSTOMER_TYPE_TEXT": this.custType,
          "PAN_NO": pan_no,
          "GST_REGISTRATION_STATUS": this.easyProtectProposal.value.gstRegStatus,
          "GST_REGISTRATION_STATUS_TEXT": this.gstName
        }
        postdata.GSTDetails = gstDetails;
      }

      localStorage.setItem('customerRequest', btoa(JSON.stringify(postdata)));
      this.cs.postWithParams('/api/customer/SaveEditCustomer', JSON.stringify(postdata)).toPromise().then((res: any) => {
        this.createCustData = res;
        localStorage.setItem('customerResponse', JSON.stringify(res));
        resolve();
      }).catch((err) => {
        this.toastr.error(err.error.Message);
        this.cs.showLoader = false;
        resolve();
      });
    });
  }


  generateProposal(): Promise<any> {
    let state_id, city_id, riskDuration, HazardCode, RiskCode, terror_rate, gstinno, burglary_data;
    city_id = this.appCityList.find(c => c.CityName == this.easyProtectProposal.value.proposalCity).CityID;
    state_id = this.stateMasterHB.find(c => c.StateName == this.easyProtectProposal.value.proposalState).StateId;
    return new Promise((resolve) => {
      if (this.isLongTermDwelling) {
        riskDuration = parseInt(this.selectedLongTermYear) * 365;
      }
      else {
        riskDuration = 365;
      }
      if (this.buttonID == 'dwellings') {
        HazardCode = '1'; RiskCode = '30101';
      }
      else if (this.buttonID == 'non_industrial') {
        HazardCode = '10'; RiskCode = '30306';
      }
      else if (this.buttonID == 'industrial') {
        HazardCode = '6'; RiskCode = '703114';
      }
      else if (this.buttonID == 'storage') {
        HazardCode = '10'; RiskCode = '62101';
      }
      if (this.occupancyData.length > 0) {
        terror_rate = this.occupancyData[0]['TERRORISM_RATE']
      }
      else {
        terror_rate = 0;
      }
      if (this.easyProtectProposal.value.GSTIN == null || this.easyProtectProposal.value.GSTIN == undefined) {
        gstinno = '';
      }
      else { gstinno = this.easyProtectProposal.value.GSTIN; }
      if (this.isbankHypothecation == false) {
        this.financierBranch = "NA";;
        this.bankName = "NA";
      }
      if (this.isBuglary) {
        burglary_data = {
          "taxType": "GST",
          "sumInsured": this.burglaryBasicPremium,
          "SIBuildings": 0,
          "SIContents": this.SIforBurglaryContents,
          "SIStocks": this.SIforBurglaryStocks,
          "basicPremium": parseFloat(this.bulgaryFinalRate).toFixed(2).toString(),
          "serviceTax": parseFloat(this.gstBuglary).toFixed(2).toString(),
          "stampDuty": 0,
          "totalPremium": this.bulgarytotalRate,
          "rate": {
            "rsmd": parseFloat(this.bulgaryCalculationData.rsmd),
            "stfi": 0,
            "eq": 0,
            "flexa": 0,
            "basic": parseFloat(this.bulgaryCalculationData.BASIC),
            "totalRate": parseFloat(this.bulgaryFinalRates.toFixed(2))
          }
        }
      }
      else {
        burglary_data = {
          "taxType": "GST",
          "SIBuildings": 0,
          "basicPremium": 0,
          "serviceTax": 0,
          "stampDuty": 0,
          "totalPremium": 0,
          "rate": {
            "stfi": 0,
            "eq": 0,
            "flexa": 0,
          }
        }
      }
      let body
      body = {
        "gstInNumber": gstinno,
        "proposalDate": moment().format('DD-MMM-YYYY'),
        "riskStartDate": moment(this.easyProtectProposal.value.riskStartDate).format('DD-MMM-YYYY'),
        "riskEndDate": moment(this.easyProtectProposal.value.riskEndDate, 'DD/MM/YYYY').format('DD-MMM-YYYY'),
        "occupancyType": "OTHERS",
        "OccupancyDesc": this.easyProtectForm.value.Occupancy,
        "HazardCode": this.occupancyData[0].hazardCode,
        "RiskCode": this.occupancyData[0].riskCode,
        "eqZone": this.zone,
        "isPropertyRented": this.easyProtectForm.value.PropertyType == 'Owned' ? false : true,
        "customerId": this.createCustData.CustomerID,
        "pfCustomerId": this.createCustData.PFCustomerID,
        "customerName": this.easyProtectProposal.value.insuredName,
        "CustomerEmailId": this.easyProtectProposal.value.applEmail,
        "CustomerMobileNo": this.easyProtectProposal.value.applMobNo,
        "customerType": this.customerType,
        "financierName": this.bankName,
        "financierBranch": this.financierBranch,
        "isHypothetication": this.isbankHypothecation,
        "firePremium": {
          "taxType": "GST",
          "sumInsured": this.valueRange,
          "SIBuildings": this.SIforBuilding,
          "SIContents": this.SIforContents,
          "SIStocks": this.SIforStocks,
          "basicPremium": parseFloat(this.totalPremium).toFixed(2).toString(),
          "serviceTax": parseFloat(this.gst).toFixed(2).toString(),
          "stampDuty": 0,
          "totalPremium": this.finalPremimum,
          "rate": {
            "rsmd": parseFloat(this.fireCalculatorData.rsmd),
            "stfi": parseFloat(this.fireCalculatorData.STFI),
            "eq": this.zoneEQRate,
            "flexa": this.isoccupancyDefault == true ? this.occupancyData[0].DEFAULT_RATE : parseFloat(this.fireCalculatorData.FLEXA),
            "basic": 0,
            "totalRate": parseFloat(this.finalRate.toFixed(2)),
            "Terriorsam_Rate": this.occupancyData[0]['TERRORISM_RATE']
          }
        },
        "riskLocationCode": "",
        "riskAddress": {
          "addressLine1": this.easyProtectProposal.value.proposalAddr1,
          "addressLine2": this.easyProtectProposal.value.proposalAddr2,
          "countryId": 100,
          "country": "INDIA",
          "stateId": state_id.toString(),
          "state": this.easyProtectProposal.value.proposalState,
          "city": this.easyProtectProposal.value.proposalCity,
          "cityId": city_id,
          "pinCode": this.easyProtectProposal.value.proposalPin,
          "pinCodeId": ""
        },

        "optingBurglary": this.isBuglary,
        "burglaryPremium": burglary_data,
        "isJammuKashmir": false,
        "firePolicy": {
          "quoteId": "",
          "quoteNumber": null,
          "prouductCode": "1001",
          "policyId": null,
          "pfProposalNo": null,
          "policyStatus": 0
        },
        "burglaryPolicy": {
          "quoteId": null,
          "quoteNumber": null,
          "prouductCode": "4002",
          "policyId": null,
          "pfProposalNo": null,
          "policyStatus": 0
        },
        "riskDuration": riskDuration,
        "corelationId": "",
        "userId": "",
        "FireRiskDesc": this.easyProtectProposal.value.fireriskdesc,
        "BurglaryRiskDesc": this.easyProtectProposal.value.buglaryriskdesc,
        "IsLongTermDwelling": this.isLongTermDwelling,
        "ISTerrorism": this.isTerrorismSelected
      };
      localStorage.setItem('proposalRequest', btoa(JSON.stringify(body)));
      this.cs.postWithParams('/api/smecorp/SaveEditProposal', JSON.stringify(body)).toPromise().then((res: any) => {
        if (res.StatusCode == '1') {
          localStorage.setItem('proposalResponse', btoa(JSON.stringify(res)));
          this.cs.showLoader = false;
          resolve();
          this.router.navigateByUrl('easy-protect-summary');
        }
        else { this.cs.showLoader = false; this.toastr.error(res.StatusMsg); }
      }).catch((err) => {
        this.toastr.error(err.error.Message);
        this.cs.showLoader = false;
        resolve();
      });
    });
  }

  createProposal() {
    if (this.buttonID == null || this.buttonID == undefined || this.buttonID == "") { this.toastr.error("Select Segment") }
    else if (this.easyProtectForm.value.Occupancy == null || this.easyProtectForm.value.Occupancy == undefined || this.easyProtectForm.value.Occupancy == '') { this.toastr.error("Search Occupancy") }
    else if (this.easyProtectForm.value.PropertyType == null || this.easyProtectForm.value.PropertyType == undefined || this.easyProtectForm.value.PropertyType == '') { this.toastr.error("Select Property Type") }
    else if (this.easyProtectForm.value.RiskPincode == null || this.easyProtectForm.value.RiskPincode == undefined || this.easyProtectForm.value.RiskPincode == '') { this.toastr.error("Enter Risk Location Pincode") }
    else if (this.easyProtectForm.value.CustomerState == null || this.easyProtectForm.value.CustomerState == undefined || this.easyProtectForm.value.CustomerState == '') { this.toastr.error("Select State") }
    else if (this.easyProtectProposal.value.insuredName == null || this.easyProtectProposal.value.insuredName == undefined || this.easyProtectProposal.value.insuredName == '') { this.toastr.error("Enter Insured Name") }
    else if (this.easyProtectProposal.value.proposalAddr1 == null || this.easyProtectProposal.value.proposalAddr1 == undefined || this.easyProtectProposal.value.proposalAddr1 == '') { this.toastr.error("Please Enter Risk Location Address Line 1") }
    else if (this.easyProtectProposal.value.proposalAddr2 == null || this.easyProtectProposal.value.proposalAddr2 == undefined || this.easyProtectProposal.value.proposalAddr2 == '') { this.toastr.error("Please Enter Risk Location Address Line 2") }
    else if (this.isCorraddSame == false) {
      if (this.easyProtectProposal.value.permAdd1 == null || this.easyProtectProposal.value.permAdd1 == undefined || this.easyProtectProposal.value.permAdd1 == '') { this.toastr.error("Please Enter Customer Address Line 1") }
      else if (this.easyProtectProposal.value.permAdd2 == null || this.easyProtectProposal.value.permAdd2 == undefined || this.easyProtectProposal.value.permAdd2 == '') { this.toastr.error("Please Enter Customer Address Line 2") }
      else if (this.easyProtectProposal.value.permPinCode == null || this.easyProtectProposal.value.permPinCode == undefined || this.easyProtectProposal.value.permPinCode == '') { this.toastr.error("Please Enter Customer Pincode") }
    }
    else if (this.easyProtectProposal.value.riskStartDate == null || this.easyProtectProposal.value.riskStartDate == undefined || this.easyProtectProposal.value.riskStartDate == '') { this.toastr.error("Please Enter Risk Start Date") }
    else if (this.easyProtectProposal.value.fireriskdesc == null || this.easyProtectProposal.value.fireriskdesc == undefined || this.easyProtectProposal.value.fireriskdesc == '') { this.toastr.error("Please Enter Risk Description for Fire") }
    else if (this.isBuglary == true && (this.easyProtectProposal.value.buglaryriskdesc == null || this.easyProtectProposal.value.buglaryriskdesc == null || this.easyProtectProposal.value.buglaryriskdesc == null)) {
      this.toastr.error("Please Enter Risk Description for Burglary")
    }
    else if (this.easyProtectProposal.value.applEmail == null || this.easyProtectProposal.value.applEmail == undefined || this.easyProtectProposal.value.applEmail == '') { this.toastr.error("Please Enter Email ID") }
    else if (this.easyProtectProposal.value.applMobNo == null || this.easyProtectProposal.value.applMobNo == undefined || this.easyProtectProposal.value.applMobNo == '') { this.toastr.error("Please Enter Mobile Number") }
    else if (this.easyProtectProposal.value.panNo != null && this.easyProtectProposal.value.panNo != undefined && this.easyProtectProposal.value.panNo != '' && !this.validatepanCard(this.easyProtectProposal.value.panNo)) {
      this.toastr.error("Please Enter Valid Pan Number");
    }
    else if (this.easyProtectProposal.value.contactName == null || this.easyProtectProposal.value.contactName == undefined || this.easyProtectProposal.value.contactName == '') { this.toastr.error("Please Enter Contact Person Name") }
    else if (this.isRegWithGST == true) {
      if (this.easyProtectProposal.value.constitution == null || this.easyProtectProposal.value.constitution == undefined || this.easyProtectProposal.value.constitution == '') { this.toastr.error("Please Select Constitution of Business") }
      else if (this.easyProtectProposal.value.customerType == null || this.easyProtectProposal.value.customerType == undefined || this.easyProtectProposal.value.customerType == '') { this.toastr.error("Please Select Customer Type") }
      else if (this.easyProtectProposal.value.gstRegStatus == null || this.easyProtectProposal.value.gstRegStatus == undefined || this.easyProtectProposal.value.gstRegStatus == '') { this.toastr.error("Please Select GST Registration Status") }
      else if (this.isGSTN == true) { if (this.easyProtectProposal.value.GSTIN == null || this.easyProtectProposal.value.GSTIN == undefined || this.easyProtectProposal.value.GSTIN == '') { this.toastr.error("Please Enter GSTIN Number") } }
      else if (this.isUIN == true) { if (this.easyProtectProposal.value.UIN == null || this.easyProtectProposal.value.UIN == undefined || this.easyProtectProposal.value.UIN == '') { this.toastr.error("Please Enter UIN Number") } }
    }
    else {
      this.cs.showLoader = true;
      this.generateCustomer().then(() => {
        this.generateProposal();
      });
    }
  }

  numberOnly(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    // console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  restrictTotalSI(val) {
    console.log(val);
    if (this.valueRange > 100000 && this.valueRange < 10000000) {
      this.quickQouteCalculation();
    }
    else {
      swal('total Sum Insured must be greater than 1 Lac nad less than 1 Cr.')
    }
  }
  selectOccupancy(val) {
    console.log(val);
  }


}
