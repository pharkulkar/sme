import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-swap',
  templateUrl: './swap.component.html',
  styleUrls: ['./swap.component.css']
})
export class SwapComponent implements OnInit {

  deal: any; decodeddeal: any;authToken: any;smeAgentData: any;
  constructor(public cs: CommonService,public router: Router, public activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getParamas();
  }

  getParamas() {
    // this.showHome = false;
    this.cs.showSwapLoader = true; window.localStorage.clear();
    this.activeRoute.queryParams.forEach((params) => {
      // console.log("Par", params);
      this.deal = encodeURIComponent(params.token);
     
      if (this.deal != undefined && this.deal != null && this.deal != '') {
        this.cs.showSwapLoader = true;
        this.getAccessTokenFromDeal(this.deal).then(() => {
            this.getCustomerDetails(this.authToken);
        });
      } else {
        // this.presentToast('Failed To autherized user...Please try again');
        this.cs.showSwapLoader = false;
      }
    });
  }

  getAccessTokenFromDeal(deal: any): Promise<any>
  {
    return new Promise((resolve) => {
     let body = {
      "ImId": deal
    }
      let str = JSON.stringify(body);
      // console.log(str);

    this.cs.getAuthToken('/api/SmeCorp/SmeCorpBasicDetails',str).then((res: any) => {
      // console.log("", res);
      if (res != '' && res != 'FAILURE') {
        this.authToken = res.AccessToken;
        this.authToken = "Basic " + this.authToken
        localStorage.setItem('authToken', JSON.stringify(this.authToken));
        this.cs.authToken = this.authToken;
        resolve();
      } else {
        alert('Something went wrong please contact to your partner');
        this.cs.showSwapLoader = false;
      }

    }).catch((err) => {
      // console.log("Error", err);
      alert('Something went wrong please contact to your partner');
      this.cs.showSwapLoader = false;
      resolve();
    });
  });
  }

  getCustomerDetails(deal: any): Promise<any> {
    return new Promise((resolve: any) => {
      this.cs.swapAppMaster('/api/smecorp/SmeCorpMaster', deal).then((res: any) => {
        // console.log("Auth", res);
        // this.showHome = true;
        localStorage.setItem('userData', btoa(JSON.stringify(res)));
        // this.smeAgentData = res;
        this.router.navigateByUrl('easy-protect');
        this.cs.showSwapLoader = false;
        resolve();
      });
    });
  }

}
