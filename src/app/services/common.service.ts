import { Injectable } from '@angular/core';
import { config } from '../config.properties';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable,of } from 'rxjs';
import { timeout, catchError, map, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  showLoader = false; authToken: any;occupancyName:any;showSwapLoader=false;

  constructor(public http: HttpClient, public router: Router) { 
     this.authToken = JSON.parse(localStorage.getItem('authToken'));
   }

  getHomeAuth(endPoint: any, data: any) {
    let userCredentials = data.userName + ":" + data.passWord;
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa(userCredentials));
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
  }

  swapAppMaster(endPoint:any, token:any){
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", token);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, '', { headers: headers }).toPromise();
  }

  getAuthToken(endPoint: any,data:any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(config.baseURL + endPoint,data, { headers: headers }).toPromise();
  }

  postWithParams(endPoint: any, body: any):Observable<any>{
    let headers = new HttpHeaders();
    // this.authToken = localStorage.getItem('authToken');
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, body, { headers: headers });
  }

  postData(endPoint: any, body: any):Observable<any>{
    this.authToken = JSON.parse(localStorage.getItem('authToken'));
    //console.log("Basic Token",this.authToken);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization",this.authToken);
    headers = headers.append('Content-Type', 'application/json');
    return this.http.post(config.baseURL + endPoint, body, { headers: headers })
    .pipe(
        catchError((err: any) => {
          return of(null);
        }),
        map((res: any) => {            
          return res;            
        }));
  }


  isUndefinedORNull(data: any) {
    if (data === undefined || data == null || data.length <= 0) {
      return true;
    } else {
      return false;
    }
  }
  getUserData(endPoint: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.http.post(config.baseURL + endPoint, {}, { headers: headers }).toPromise();
  }
  pdfDownload(type: any, policID: any) {
    return config.baseURL + '/api/smecorp/ViewPolicyPDF?policyId=' + encodeURIComponent(policID);
  }

  save(file: string, fileName: string) {
    // console.log(file);
    if (window.navigator.msSaveOrOpenBlob) {
      navigator.msSaveBlob(file, fileName);
    } else {
      // console.log('creation');
      const downloadLink = document.createElement("a");
      downloadLink.style.display = "none";
      document.body.appendChild(downloadLink);
      downloadLink.setAttribute("href", file);
      downloadLink.setAttribute("download", fileName);
      downloadLink.click();
      document.body.removeChild(downloadLink);
      // console.log('clicked');
    }
  }
  postUserData(endPoint: any,body:any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append("Content-Type", "application/x-www-form-urlencoded");
    return this.http.post(config.baseURL + endPoint,body, { headers: headers }).toPromise();
  }

  GenerateHtNumber(endPoint: any, body: any) {
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", this.authToken);
    headers = headers.append("Content-Type", "application/json");
    return this.http.post(config.baseURL + endPoint, body, { headers: headers }).toPromise();
  }

  uploadImageToDMS(endPoint: any, body: any) {
    return this.http.post(config.baseURL + endPoint, body).toPromise();
  }




}
