import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { EasyProtectComponent } from './pages/easy-protect/easy-protect.component';
import { SummaryComponent } from './pages/summary/summary.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ModalcontrollerComponent } from './modalcontroller/modalcontroller.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatFormFieldModule, MatRadioModule,MatSelectModule, MatNativeDateModule,MAT_DATE_LOCALE  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { Component } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { PaymentComponent } from './pages/payment/payment.component';
import { PaymentConfirmationComponent } from './pages/payment-confirmation/payment-confirmation.component';
import { RazorPayFallbackComponent } from './pages/razor-pay-fallback/razor-pay-fallback.component';
import { SwapComponent } from './pages/swap/swap.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EasyProtectComponent,
    SummaryComponent,
    ModalcontrollerComponent,
    PaymentComponent,
    PaymentConfirmationComponent,
    RazorPayFallbackComponent,
    SwapComponent,
  ],
  exports: [
    ModalcontrollerComponent
  ],
  entryComponents:[
    ModalcontrollerComponent
  ],

  imports: [
    Ng2SearchPipeModule,
    MatSlideToggleModule,
    MatSidenavModule,
    MatSliderModule,
    BrowserModule,
    MatDialogModule,
    MatRadioModule,
    MatSelectModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    MatExpansionModule,
    MatTabsModule,
    MatInputModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
