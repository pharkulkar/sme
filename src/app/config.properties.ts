export var serviceURL = {
    // Sanity
    // ipartner : 'https://cldiliptrapp01.cloudapp.net/WebPages/',
    // mobility : 'http://cldiliptrapp03.cloudapp.net:9006/',
    // biztalk : 'https://cldilbizapp02.cloudapp.net:9001',
    // mobileVD : 'MobileAPI/', //MobileAPIUAT //MobileAPI,
    // razorPayURL: 'http://cldiliptrapp03.cloudapp.net:9006'

    // Prod
    ipartner: 'https://ipartner.icicilombard.com/WebPages/',
    mobility: 'https://ipartner.icicilombard.com/',
    biztalk: 'https://app9.icicilombard.com',
    mobileVD: 'mobileagentapi/',
    razorPayURL: 'https://ipartner.icicilombard.com',
    paymentURL: 'https://ipartner.icicilombard.com/mobileagentapi/ipartnermsite/easyprotect/#/easy-protect-payment'

    //UAT
    // ipartner: 'https://cldiliptrapp03.cloudapp.net/WebPages/',
    // mobility: 'http://cldiliptrapp03.cloudapp.net:9006/',
    // biztalk: 'https://cldilbizapp02.cloudapp.net:9001',
    // mobileVD: 'MobileAPIUAT',
    // razorPayURL: 'http://cldiliptrapp03.cloudapp.net:9006',
    // paymentURL: 'http://cldiliptrapp03.cloudapp.net:9006/ipartnermsite/easyprotect/#/easy-protect-payment'
}
export var config = {
    ipAddress: "SME-MSITE",
    baseURL: 'https://ipartner.icicilombard.com/mobileagentapi',
    //baseURL: 'http://cldiliptrapp03.cloudapp.net:9006/MobileAPIUAT',
    razorPayURL: 'http://cldiliptrapp03.cloudapp.net:9006',
    razorPayFallBackURL: serviceURL.mobility + serviceURL.mobileVD,
    paymentURL: serviceURL.paymentURL,


    //  Origin-2
    // baseURL: 'https://origin2-ipartner.icicilombard.com/mobileagentapi',
}