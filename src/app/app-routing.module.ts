import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { EasyProtectComponent } from './pages/easy-protect/easy-protect.component';
import { SummaryComponent } from './pages/summary/summary.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { PaymentConfirmationComponent } from './pages/payment-confirmation/payment-confirmation.component'
import { RazorPayFallbackComponent } from './pages/razor-pay-fallback/razor-pay-fallback.component'
import { SwapComponent } from './pages/swap/swap.component'
const routes: Routes = [
    { path: '', redirectTo: 'swap', pathMatch: 'full' },
    { path: 'swap', component: SwapComponent },
    { path: 'home', component: HomeComponent },
    { path: 'easy-protect', component: EasyProtectComponent },
    { path: 'easy-protect-summary', component: SummaryComponent },
    { path: 'easy-protect-payment', component: PaymentComponent},
    { path: 'easy-protect-payment-confirmation', component: PaymentConfirmationComponent},
    { path: 'easy-protect-razorpay-fallback', component: RazorPayFallbackComponent},
  ];

  @NgModule({
    imports: [
      RouterModule.forRoot(routes, {useHash: true})
    ],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }